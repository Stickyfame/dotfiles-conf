set nu
set autoindent
set expandtab
set shiftwidth=4
set softtabstop=4

call plug#begin('~/.vim/plugged')

Plug 'drewtempelmeyer/palenight.vim'
Plug 'ayu-theme/ayu-vim'
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'hashivim/vim-terraform'
" Plug 'airblade/vim-gitgutter'

call plug#end()

" palenight colorscheme
set termguicolors
let g:airline_theme = 'palenight'
colorscheme palenight

let g:terraform_fmt_on_save = 1

" ayu colorscheme
"set termguicolors     " enable true colors support
"let ayucolor="light"  " for light version of theme
"let ayucolor="mirage" " for mirage version of theme
"let ayucolor="dark"   " for dark version of theme
"colorscheme ayu

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_cpp_compiler_options = ' --std=c++11'
let g:syntastic_cpp_include_dirs = ['headers/']
let g:syntastic_c_include_dirs = ['headers/']

map <C-n> :NERDTreeToggle<CR>

set updatetime=1000

let g:DoxygenToolkit_paramTag_pre="@param "
let g:DoxygenToolkit_returnTag="@returns "
let g:DoxygenToolkit_authorName="Charles Anteunis"
